/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enriL.tarea4server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author HABE(osea soy Enri losers) Espero que les sirva compañeras/os, creen
 * un main y desde ahi instancian esta clase.. no es nada complicado pero deben
 * de tener una base de datos compo prueba
 */
public class conexionbd {

    static String bd = "sgcr"; // nombre de la base de datos
    static String login = "root"; // usuario gestor de la base de datos
    static String password = "12345"; // contraseña del gestor de BD
    static String url = "jdbc:mysql://localhost/" + bd;//para no estar escribiendo el chorizo de codigo lo simplifico dividiendo en cadenas, tambien es valido colocar estas sintaxis una tras otra.
    Connection conn = null;

    /**
     * Constructor de conexionbd()
     */
    public conexionbd() {
        try {
            //obtenemos el driver para mysql
            Class.forName("com.mysql.jdbc.Driver");
            //obtenemos la conexiion
            conn = (Connection) DriverManager.getConnection(url, login, password);//ven . que les dije . es mas corto asi.
            if (conn != null) {
                System.out.println("Conexión a base de datos " + bd + " OK");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
    }

    /**
     * Permite retornar la conexion
     */
    public Connection getConnection() {
        return conn;
    }

    public void desconectar() {
        conn = null;
    }
}
