/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enriL.tarea3cicl;

import javax.swing.*;
import java.awt.event.*;
import java.awt.Toolkit;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class tempinterna {

    public static void main(String[] args) {

        Reloj mireloj = new Reloj(3000, true);
        mireloj.enmarcha();
        JOptionPane.showMessageDialog(null, "Pulsa aceptar para terminar");
        System.exit(0);

    }

}

class Reloj {

    public Reloj(int intervalo, boolean sonido) {
        this.intervalo = intervalo;
        this.sonido = sonido;
    }

    public void enmarcha() {
        ActionListener oyente = new DameLaHora2();
        Timer mitemporizador = new Timer(intervalo, oyente);
        mitemporizador.start();
    }
    private int intervalo;
    private boolean sonido;
//////comienzo de clase interna/////////

    public class DameLaHora2 implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            Date ahora = new Date();
            System.out.println("Te pongo la hora cada 3 se" + ahora);
            if (sonido) {
                Toolkit.getDefaultToolkit().beep();
            }
        }
    }
    //////Fin de clase interna/////////
}
