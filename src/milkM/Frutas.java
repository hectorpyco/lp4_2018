/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milkM;


public abstract class Frutas {
    private String color;
    private String funcionmedicinal;
    private String estado;

    public Frutas() {
    }

    public abstract String getColor();

    public abstract void setColor();

    public abstract String getFuncionmedicinal();

    public abstract void setFuncionmedicinal();

    public abstract String getEstado();
    
    public abstract void setEstado();
  
}
